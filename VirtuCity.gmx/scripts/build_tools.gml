///build_tools(type,subtype);
switch(argument0)
{
 case ROAD:
  switch(argument1)
   {
    case UNPAVED:
    build_tool = UNPAVED;
    build_image = spr_build_unpaved_road;
    break;
    case ASPHALT:   
    build_tool = ASPHALT;  
    build_image = spr_build_road;
    break;
    case LIGHTS:
    build_tool = LIGHTS;
    build_image = spr_build_light_road;
    break;
    default:
    build_tool = NONE;
    build_image = spr_terrain_highlight;
    break;
   }
  break;
  
  case ZONES:
   switch(argument1)
    {
     case RESIDENTIAL:
     build_tool = RESIDENTIAL;
     build_image = spr_build_residential_t;
     break;
     case COMMERCIAL:
     build_tool = COMMERCIAL;
     build_image = spr_build_commercial_t;
     break;
     case INDUSTRIAL:
     build_tool = INDUSTRIAL;
     build_image = spr_build_industrial_t;
     break;
     default:
     build_tool = NONE;
     build_image = spr_terrain_highlight;
     break;
    }
  break;
  
  case WATER:
   switch(argument1)
    {
    case PUMP:
    build_tool = PUMP;
    build_image = spr_build_water_pump;
    break;
    case TOWER:
    build_tool = TOWER;
    build_image = spr_build_water_tower;
    break;
    default:
    build_tool = NONE;
    build_image = spr_terrain_highlight;
    break;
    }
   break;
  
  case ELECTRICITY:
   switch(argument1)
    {
    case TURBINE:
    build_tool = TURBINE;
    build_image = spr_build_wind_turbine;
    break;
    default:
    build_tool = NONE;
    build_image = spr_terrain_highlight;
    break;
    }
  break;
  
  default:
  build_tool = NONE;
  build_image = spr_terrain_highlight;
  break;
}



















