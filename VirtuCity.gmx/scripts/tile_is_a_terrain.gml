///tile_is_a_terrain(type,location)
if tile_exists(argument1)
{
switch(argument0)
{
 case TERRAIN:
  switch(tile_get_background(argument1))
   {
    case terrain_b_center_slope_back:
    return 1;
    break;
    case terrain_b_center_slope_front:
    return 2;
    break;
    case terrain_b_center_slope_left:
    return 3;
    break;
    case terrain_b_center_slope_right:
    return 4;
    break;
    case terrain_b_flat_bot_back:
    return 5;
    break;
    case terrain_b_flat_bot_front:
    return 6;
    break;
    case terrain_b_flat_bot_left:
    return 7;
    break;
    case terrain_b_flat_bot_right:
    return 8;
    break;
    case terrain_b_flat_top_back:
    return 9;
    break;
    case terrain_b_flat_top_front:
    return 10;
    break;
    case terrain_b_flat_top_left:
    return 11;
    break;
    case terrain_b_flat_top_right:
    return 12;
    break;
    case terrain_b_slope_back_left:
    return 13;
    break;
    case terrain_b_slope_back_right:
    return 14;
    break;
    case terrain_b_slope_right:
    return 15;
    break;
    case terrain_b_slope_left:
    return 16;
    break;
    case terrain_a_grass_flat:
    return 17;
    break;
   }
}
}
