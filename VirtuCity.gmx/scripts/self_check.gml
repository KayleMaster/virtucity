///self_check(LocalX,LocalY,build)
var arg=argument0,argg=argument1;
if tile_exists(chunk[# arg,argg])
{
if argument2==true{
if tile_get_background(chunk[# arg,argg])==terrain_a_grass_flat
 {
 tile_delete(chunk[# arg,argg]);
 chunk[# arg,argg] = tile_add(unpaved_road_a,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
 times_built += 1
 }}

var c_south=0,c_north=0,c_west=0,c_east=0;

if tile_is_a_road(UNPAVED,chunk[# arg,argg+1])>0//south of the tile
 {
 c_south=1;
 }
if tile_is_a_road(UNPAVED,chunk[# arg,argg-1])>0//north of the tile
 {
 c_north=1;
 }
if tile_is_a_road(UNPAVED,chunk[# arg+1,argg])>0//east of the tile
 {
 c_east=1;
 }
if tile_is_a_road(UNPAVED,chunk[# arg-1,argg])>0//west of the tile
 {
 c_west=1;
 }
 
if tile_is_a_road(UNPAVED,chunk[# arg,argg])>0 {
if c_south and c_north and c_east and c_west //make a 4 way
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_4_crossing,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}

if c_south and c_north and c_east and !c_west //make a 3 way
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_3_crossing_4,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if c_south and c_north and !c_east and c_west //make a 3 way
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_3_crossing,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if c_south and !c_north and c_east and c_west //make a 3 way
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_3_crossing_3,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if !c_south and c_north and c_east and c_west //make a 3 way
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_3_crossing_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}

if c_south and c_north and !c_east and !c_west //make a straight north-south
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_a,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if c_south and !c_north and c_east and !c_west //make a turn east south
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_turn,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if !c_south and c_north and c_east and !c_west //make a turn east north
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_turn_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if c_south and !c_north and !c_east and c_west //make a turn west south
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_turn_4,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if !c_south and c_north and !c_east and c_west //make a turn west north
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_turn_3,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if !c_south and !c_north and c_east and c_west //make a straight east-west
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}

if !c_south and !c_north and !c_east and c_west//if only from west
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if !c_south and !c_north and c_east and !c_west //if only from east
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if !c_south and c_north and !c_east and !c_west //if only from north
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_a,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}
if c_south and !c_north and !c_east and !c_west //if only from south
{tile_delete(chunk[# arg,argg]); chunk[# arg,argg]=tile_add(unpaved_road_a,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);}

}
 
 
}
