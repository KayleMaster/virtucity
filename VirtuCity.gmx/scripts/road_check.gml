///road_check(LocalX,LocalY)
arg = argument0; argg = argument1;
var calc;
if tile_exists(chunk[# arg,argg])
 {if tile_get_background(chunk[# arg,argg]) == terrain_a_grass_flat
  {
   tile_delete(chunk[# arg,argg]);
   chunk[# arg,argg] = tile_add(unpaved_road_a,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
  }
 }
 // ------------------------------------------SOUTH--------------------------------
if tile_exists(chunk[# arg,argg+1]) 
 {
  switch(tile_is_a_road(UNPAVED,chunk[# arg,argg+1])) //south of tile
   {
   case 1: //Straight road north-south
   break;
   
   case 2: //Turn north-east
   break;
   
   case 3: //Turn north-west  

   break;
   
   case 4: //Turn south-west
   tile_delete(chunk[# arg,argg+1]);
   chunk[# arg,argg+1] = tile_add(unpaved_road_3_crossing,0,0,128,64,IsoToScreenX(arg,argg+1),IsoToScreenY(arg,argg+1),-1);
   break;
   
   case 5: //Turn south-east
   tile_delete(chunk[# arg,argg+1]);
   chunk[# arg,argg+1] = tile_add(unpaved_road_3_crossing_4,0,0,128,64,IsoToScreenX(arg,argg+1),IsoToScreenY(arg,argg+1),-1);
   break;
   
   case 6: //3 way without south
   case 7: //3 way without north
   tile_delete(chunk[# arg,argg+1]);
   chunk[# arg,argg+1] = tile_add(unpaved_road_4_crossing,0,0,128,64,IsoToScreenX(arg,argg+1),IsoToScreenY(arg,argg+1),-1);
   break;
   case 8: //3 way without west
   case 9: //3 way without east  
   break;
   case 10: //4 way
   break;
   
   case 11://Straight road east-west
   
   //tile_delete(chunk[# arg,argg]);
   //chunk[# arg,argg] = tile_add(unpaved_road_turn_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
   //------------------------
    var con_east=0,con_west=0;
   if tile_exists(chunk[# arg+1,argg+1]) //east of THAT tile
    {
      if tile_is_a_road(UNPAVED,chunk[# arg+1,argg+1])>0 
      {
       con_east = true;
      }
    }
   if tile_exists(chunk[# arg-1,argg+1]) // west of THAT tile
       {
         if tile_is_a_road(UNPAVED,chunk[# arg-1,argg+1])>0 
          {
           con_west = true;
          }
       }
   if con_west and con_east
    {
     tile_delete(chunk[# arg,argg+1]);
     chunk[# arg,argg+1] = tile_add(unpaved_road_3_crossing_2,0,0,128,64,IsoToScreenX(arg,argg+1),IsoToScreenY(arg,argg+1),-1);

    }
   if con_west and !con_east
    {
     tile_delete(chunk[# arg,argg+1]); 
     chunk[# arg,argg+1] = tile_add(unpaved_road_turn_3,0,0,128,64,IsoToScreenX(arg,argg+1),IsoToScreenY(arg,argg+1),-1);
    }
   if !con_west and con_east
    {
     tile_delete(chunk[# arg,argg+1]); 
     chunk[# arg,argg+1] = tile_add(unpaved_road_turn_2,0,0,128,64,IsoToScreenX(arg,argg+1),IsoToScreenY(arg,argg+1),-1);
    }
    //-------------------------------------
   break;
   
   }
 }
 // ------------------------------------------WEST--------------------------------
 if tile_exists(chunk[# arg-1,argg]) 
 {
  switch(tile_is_a_road(UNPAVED,chunk[# arg-1,argg])) //west of tile
   {
   case 1: //Straight road north-south
   var con_south=0,con_north=0;
   if tile_exists(chunk[# arg-1,argg-1]) //north of THAT tile
    {
      if tile_is_a_road(UNPAVED,chunk[# arg-1,argg-1])>0 
      {
       con_north = true;
      }
    }
   if tile_exists(chunk[# arg-1,argg+1]) // south of THAT tile
       {
         if tile_is_a_road(UNPAVED,chunk[# arg-1,argg+1])>0 
          {
           con_south = true;
          }
       }
   if con_south and con_north
    {
     tile_delete(chunk[# arg-1,argg]);
     chunk[# arg-1,argg] = tile_add(unpaved_road_3_crossing_4,0,0,128,64,IsoToScreenX(arg-1,argg),IsoToScreenY(arg-1,argg),-1);
     //TODO FIX
     tile_delete(chunk[# arg,argg]);
     chunk[# arg,argg] = tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
    }
   if con_south and !con_north
    {
     tile_delete(chunk[# arg-1,argg]); 
     chunk[# arg-1,argg] = tile_add(unpaved_road_turn,0,0,128,64,IsoToScreenX(arg-1,argg),IsoToScreenY(arg-1,argg),-1);
     //TODO FIX
     tile_delete(chunk[# arg,argg]);
     chunk[# arg,argg] = tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
    }
   if !con_south and con_north
    {
     tile_delete(chunk[# arg-1,argg]); 
     chunk[# arg-1,argg] = tile_add(unpaved_road_turn_2,0,0,128,64,IsoToScreenX(arg-1,argg),IsoToScreenY(arg-1,argg),-1);
     //TODO FIX
     tile_delete(chunk[# arg,argg]);
     chunk[# arg,argg] = tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
    }
   break;
   
   case 2: //Turn north-east
   break;
   
   case 3: //Turn north-west  
   tile_delete(chunk[# arg-1,argg]);
   chunk[# arg-1,argg] = tile_add(unpaved_road_3_crossing_2,0,0,128,64,IsoToScreenX(arg-1,argg),IsoToScreenY(arg-1,argg),-1);
   break;
   
   case 4: //Turn south-west
   tile_delete(chunk[# arg-1,argg]);
   chunk[# arg-1,argg] = tile_add(unpaved_road_3_crossing_3,0,0,128,64,IsoToScreenX(arg-1,argg),IsoToScreenY(arg-1,argg),-1);
   break;
   
   case 5: //Turn south-east
   break;
   
   case 6: //3 way without south
   case 7: //3 way without north
   case 8: //3 way without west
   break;
   case 9: //3 way without east
   tile_delete(chunk[# arg-1,argg]);
   chunk[# arg-1,argg] = tile_add(unpaved_road_4_crossing,0,0,128,64,IsoToScreenX(arg-1,argg),IsoToScreenY(arg-1,argg),-1);
   break;
   case 10: //4 way TODO FIX AND EXPAND
   tile_delete(chunk[# arg,argg]);
   chunk[# arg,argg] = tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
   break;
   
   case 11://Straight road east-west
   tile_delete(chunk[# arg,argg]);
   chunk[# arg,argg] = tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
   break;
   
   }
 }
 
  // ------------------------------------------NORTH--------------------------------
if tile_exists(chunk[# arg,argg-1]) 
 {
  switch(tile_is_a_road(UNPAVED,chunk[# arg,argg-1])) //north of tile
   {
   case 1: //Straight road north-south
   break;
   
   case 2: //Turn north-east
   tile_delete(chunk[# arg,argg-1]);
   chunk[# arg,argg-1] = tile_add(unpaved_road_3_crossing_4,0,0,128,64,IsoToScreenX(arg,argg-1),IsoToScreenY(arg,argg-1),-1);  
   break;
   
   case 3: //Turn north-west  
   tile_delete(chunk[# arg,argg-1]);
   chunk[# arg,argg-1] = tile_add(unpaved_road_3_crossing,0,0,128,64,IsoToScreenX(arg,argg-1),IsoToScreenY(arg,argg-1),-1);
   break;
   
   case 4: //Turn south-west
   break;
   
   case 5: //Turn south-east
   break;
   
   case 6: //3 way without south
   tile_delete(chunk[# arg,argg-1]);
   chunk[# arg,argg-1] = tile_add(unpaved_road_4_crossing,0,0,128,64,IsoToScreenX(arg,argg-1),IsoToScreenY(arg,argg-1),-1);
   break;
   
   case 7:
   case 8:
   case 9:
   case 10:
   break;
   
   case 11://Straight road east-west
   tile_delete(chunk[# arg,argg-1]);
   chunk[# arg,argg-1] = tile_add(unpaved_road_3_crossing_3,0,0,128,64,IsoToScreenX(arg,argg-1),IsoToScreenY(arg,argg-1),-1);
   break;         
   }
 }
 
 
 /*
 var con_south=0,con_north=0;
   if tile_exists(chunk[# arg,argg+1-1]) //north of THAT tile
    {
      if tile_is_a_road(UNPAVED,chunk[# arg,argg+1-1])>0 
      {
       con_north = true;
      }
    }
   if tile_exists(chunk[# arg,argg+1+1]) // south of THAT tile
       {
         if tile_is_a_road(UNPAVED,chunk[# arg,argg+1+1])>0 
          {
           con_south = true;
          }
       }
   if con_south and con_north
    {
     tile_delete(chunk[# arg,argg+1]);
     chunk[# arg,argg+1] = tile_add(unpaved_road_3_crossing_4,0,0,128,64,IsoToScreenX(arg,argg+1),IsoToScreenY(arg,argg+1),-1);
     //TODO FIX
     tile_delete(chunk[# arg,argg]);
     chunk[# arg,argg] = tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
    }
   if con_south and !con_north
    {
     tile_delete(chunk[# arg,argg+1]); 
     chunk[# arg,argg+1] = tile_add(unpaved_road_turn,0,0,128,64,IsoToScreenX(arg,argg+1),IsoToScreenY(arg,argg+1),-1);
     //TODO FIX
     tile_delete(chunk[# arg,argg]);
     chunk[# arg,argg] = tile_add(unpaved_road_a_2,0,0,128,64,IsoToScreenX(arg,argg),IsoToScreenY(arg,argg),-1);
    }
 
 
 
 
 
 
 
 
 
 
 
