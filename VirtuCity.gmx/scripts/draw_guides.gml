///draw_guides()
if pressed_r == 1
{
   var xd = IsoToScreenX(lxx+1,lyy);
   var yd = IsoToScreenY(lxx+1,lyy);
   

 //if mouse_y >= lpy //south
 if point_direction(mouse_x,mouse_y,xd,yd-32)>26.65  and point_direction(mouse_x,mouse_y,xd,yd-32)<153.33
  {
    times_x = LocalX - lxx;
    times_y = LocalY - lyy;
    //total_times = (times_x + times_y);
   var xd = IsoToScreenX(lxx+1,lyy);
   var yd = IsoToScreenY(lxx+1,lyy);
   //var xh = IsoToScreenX(lxx+1+times_x,lyy);
   //var yh = IsoToScreenY(lxx+1,lyy+times_y);
   var xf = IsoToScreenX(lxx+1+times_x,lyy+times_y);
   var yf = IsoToScreenY(lxx+1+times_x,lyy+times_y);
   draw_line(xd,yd-32,xd+64+(times_x*64),yd+(times_x*32));
   draw_line(xd,yd-32,xd-64-(times_y*64),yd+(times_y*32));
   
   draw_line(xd+64+(times_x*64),yd+(times_x*32),xf,yf+32);
   draw_line(xd-64-(times_y*64),yd+(times_y*32),xf,yf+32);
  }
 //if mouse_y < lpy /*north*/ 
   var xd = IsoToScreenX(lxx+1,lyy);
   var yd = IsoToScreenY(lxx+1,lyy);
 if point_direction(mouse_x,mouse_y,xd,yd+32)>206.63  and point_direction(mouse_x,mouse_y,xd,yd+32)<333.28
 //if (point_direction(xd,yd+32,xd+64-(times_y*64),yd+(times_y*32)) 
 // + point_direction(xd,yd+32,xd-64+(times_x*64),yd+(times_x*32)))
 // == (point_direction(mouse_x,mouse_y,xd+64-(times_y*64),yd+(times_y*32))+ point_direction(mouse_x,mouse_y,xd-64+(times_x*64),yd+(times_x*32)))
  {
    times_x = LocalX - lxx;
    times_y = LocalY - lyy;
    //total_times = (times_x + times_y);
   var xd = IsoToScreenX(lxx+1,lyy);
   var yd = IsoToScreenY(lxx+1,lyy);
   //var xh = IsoToScreenX(lxx+1+times_x,lyy);
   //var yh = IsoToScreenY(lxx+1,lyy+times_y);
   var xf = IsoToScreenX(lxx+1+times_x,lyy+times_y);
   var yf = IsoToScreenY(lxx+1+times_x,lyy+times_y);
   draw_line(xd,yd+32,xd+64-(times_y*64),yd+(times_y*32));
   draw_line(xd,yd+32,xd-64+(times_x*64),yd+(times_x*32));
   
   draw_line(xd+64-(times_y*64),yd+(times_y*32),xf,yf-32);
   draw_line(xd-64+(times_x*64),yd+(times_x*32),xf,yf-32);
  }
  
 /* var xd = IsoToScreenX(lxx+1,lyy);
   var yd = IsoToScreenY(lxx+1,lyy);
   draw_text(100,100,(point_direction(xd+64,yd,xd+64+(times_x*64),yd+(times_x*32))));
   draw_text(100,200,(point_direction(mouse_x,mouse_y,xd+64,yd)));
 if point_direction(mouse_x,mouse_y,xd,yd-32)<=26.65  and point_direction(mouse_x,mouse_y,xd,yd+32)>=333.28

  {
    times_x = LocalX - lxx;
    times_y = LocalY - lyy;

   var xd = IsoToScreenX(lxx+1,lyy);
   var yd = IsoToScreenY(lxx+1,lyy);

   var xf = IsoToScreenX(lxx+1+times_x,lyy+times_y);
   var yf = IsoToScreenY(lxx+1+times_x,lyy+times_y);
   draw_line(xd+64,yd,xd-(times_y*64),yd+32+(times_y*32));
   draw_line(xd+64,yd,xd+(times_x*64),yd-32+(times_x*32));
   
  // draw_line(xd+64-(times_y*64),yd+(times_y*32),xf,yf-32);
  // draw_line(xd-64+(times_x*64),yd+(times_x*32),xf,yf-32);
  }*/
}

if pressed == 1
{
    
 if mouse_x > lpx and mouse_y >= lpy //east
  {
    times_x = round((abs((lpx-mouse_x)/128))+0.5);
    times_y = round((abs((lpy-mouse_y)/64)));
    total_times = (times_x + times_y);
   var xd = IsoToScreenX(lxx+1,lyy);
   var yd = IsoToScreenY(lxx+1,lyy);
   var xf = IsoToScreenX(lxx+1+total_times,lyy);
   var yf = IsoToScreenY(lxx+1+total_times,lyy);
   
   draw_line(xd,yd-32,xf,yf-32);
   draw_line(xd-64,yd,xf-64,yf);
   
   draw_line(xf-64,yf,xf,yf-32);
   draw_line(xd-64,yd,xd,yd-32);
   draw_set_font(f_roboto2);
   draw_text(xf-64,yf-32,total_times);
   draw_set_font(f_roboto1);
   draw_set_color(c_red);
   draw_text(xf-64,yf+20,"-"+string(total_times*cost(ROAD,UNPAVED))+"$");
   draw_set_color(c_white);
 }
 if mouse_x < lpx+1 and mouse_y < lpy //west
  {
    times_x = round((abs((lpx+1-mouse_x)/128))+0.5);
    times_y = round((abs((lpy-mouse_y)/64)));
    total_times = (times_x + times_y);
   var xf = IsoToScreenX(lxx+1,lyy);
   var yf = IsoToScreenY(lxx+1,lyy);
   var xd = IsoToScreenX(lxx+1-total_times,lyy);
   var yd = IsoToScreenY(lxx+1-total_times,lyy);
   draw_line(xd+64,yd,xf+64,yf);
   draw_line(xd,yd+32,xf,yf+32);
   
   draw_line(xf,yf+32,xf+64,yf);
   draw_line(xd,yd+32,xd+64,yd);

   draw_set_font(f_roboto2);
   draw_text(xd+64,yd+32,total_times);
   draw_set_font(f_roboto1);
   draw_set_color(c_red);
   draw_text(xd+64,yd-20,"-"+string(total_times*cost(ROAD,UNPAVED))+"$");
   draw_set_color(c_white);
 }
  if mouse_x <= lpx and mouse_y >= lpy //south
  {
    times_x = round((abs((lpx-mouse_x)/128)));
    times_y = round(((abs((lpy-mouse_y)/64)))-0.5);
    total_times = (times_x + times_y);
   var xf = IsoToScreenX(lxx+1,lyy);
   var yf = IsoToScreenY(lxx+1,lyy);
   var xd = IsoToScreenX(lxx+1,lyy+total_times);
   var yd = IsoToScreenY(lxx+1,lyy+total_times);
   draw_line(xf,yf-32,xd-64,yd);
   draw_line(xf+64,yf,xd,yd+32);
   
   draw_line(xf,yf-32,xf+64,yf);
   draw_line(xd-64,yd,xd,yd+32);

   draw_set_font(f_roboto2);
   draw_text(xd,yd,total_times+1);
   draw_set_font(f_roboto1);
   draw_set_color(c_red);
   draw_text(xd,yd+20+32,"-"+string((total_times+1)*cost(ROAD,UNPAVED))+"$");
   draw_set_color(c_white);
 }
  if mouse_x > lpx and mouse_y < lpy //north
  {
    times_x = round((abs((lpx-mouse_x)/128)));
    times_y = round(((abs((lpy-mouse_y)/64)))-0.5);
    total_times = (times_x + times_y);
   var xd = IsoToScreenX(lxx+1,lyy);
   var yd = IsoToScreenY(lxx+1,lyy);
   var xf = IsoToScreenX(lxx+1,lyy-total_times);
   var yf = IsoToScreenY(lxx+1,lyy-total_times);
   draw_line(xf,yf-32,xd-64,yd);
   draw_line(xf+64,yf,xd,yd+32);
   
   draw_line(xf,yf-32,xf+64,yf);
   draw_line(xd-64,yd,xd,yd+32);

   draw_set_font(f_roboto2);
   draw_text(xf,yf,total_times+1);
   draw_set_font(f_roboto1);
   draw_set_color(c_red);
   draw_text(xf,yf-20-32,"-"+string((total_times+1)*cost(ROAD,UNPAVED))+"$");
   draw_set_color(c_white);
 }
}
